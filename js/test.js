let deck = []; // Deck
const maxDeck = 52; // Nombre de cartes dans mon deck

let calc, cpt; // Variables flottantes

let body = document.querySelector("body");
let deckContainer = document.querySelector(".deck-container");

// Containers pour affichage

let mainJ1Container = document.querySelector(".main-j1-container");
/*let mainJ2Container = document.querySelector(".main-j2-container");
let mainJ3Container = document.querySelector(".main-j3-container");
let mainJ4Container = document.querySelector(".main-j4-container"); */

let divCartesCommunes = document.querySelector("#cartes-communes");

// Création des objets Score pour chaque joueur

const Score = function() {

    this.figure = null;
    this.valeur = [];
    this.couleur = null;
    this.kicker = [];

};

const scoreJ1 = new Score();
const scoreJ2 = new Score();
const scoreJ3 = new Score();
const scoreJ4 = new Score();

// Mélange des cartes

for (i=0 ; i < maxDeck ; i++) {

    let deckBreak = false;

    do{

        calc = Math.floor( Math.random() * maxDeck );

        if (deck[calc]==undefined) {
            deck[calc] = i;
            deckBreak = true;
        }

    } while (deckBreak == false);

};

// Attribution des cartes

const cardsJ1 = [deck[0],deck[4]];
const cardsJ2 = [deck[1],deck[5]];
const cardsJ3 = [deck[2],deck[6]];
const cardsJ4 = [deck[3],deck[7]];

const cardsCommon = [ deck[9], deck[10], deck[11], deck[13], deck[15] ];

// Affichage Cartes CSS


printCards(cardsJ1, mainJ1Container);
/* printCards(cardsJ2, mainJ2Container);
printCards(cardsJ3, mainJ3Container);
printCards(cardsJ4, mainJ4Container); 
*/

printCards(cardsCommon, divCartesCommunes);
