let deck = []; // Deck
const maxDeck = 52; // Nombre de cartes dans mon deck

let calc, cpt; // Variables flottantes

let body = document.querySelector("body");
let deckContainer = document.querySelector(".deck-container");

// Containers pour affichage

let mainJ1Container = document.querySelector(".main-j1-container");
let mainJ2Container = document.querySelector(".main-j2-container");
let mainJ3Container = document.querySelector(".main-j3-container");
let mainJ4Container = document.querySelector(".main-j4-container");

let divCartesCommunes = document.querySelector(".cartes-communes");

let scoreContainer = document.querySelector(".score-container");

let resultatContainer = document.querySelector(".resultat-container");

// Dictionnaires

let nomFigure = ["-ERREUR-", "Carte Haute", "Paire", "Deux Paires", "Brelan", "Suite", "Couleur", "Full", "Carré", "Quinte Flush", "Quinte Flush Royale"];
let nomCouleur = ["Trèfle", "Carreau", "Coeur", "Pique"];
let nomValeur = [null, null, "Deux", "Trois", "Quatre", "Cinq", "Six", "Sept", "Huit", "Neuf", "Dix", "Valet", "Dame", "Roi", "As"];

// Variables

let interrupteurs = [J1Couche = false, J2Couche = false, J3Couche = false, J4Couche = false];

// Création des objets Score pour chaque joueur

/*
var scoreJ1 = createScoreObj (scoreJ1);
var scoreJ2 = createScoreObj (scoreJ2);
var scoreJ3 = createScoreObj (scoreJ3);
var scoreJ4 = createScoreObj (scoreJ4);
*/

const Score = function() {

    this.figure = null;
    this.valeur = [];
    this.couleur = null;
    this.kicker = [];

};

const scoreJ1 = new Score();
const scoreJ2 = new Score();
const scoreJ3 = new Score();
const scoreJ4 = new Score();

// Mélange des cartes

for (i=0 ; i < maxDeck ; i++) {

    let deckBreak = false;

    do{

        calc = Math.floor( Math.random() * maxDeck );

        if (deck[calc]==undefined) {
            deck[calc] = i;
            deckBreak = true;
        }

    } while (deckBreak == false);

};

// Affichage du tirage


for (i=0 ; i < maxDeck ; i++) {
    // console.log(deck[i]);
    let spanDeck= document.createElement("span");
    deckContainer.appendChild(spanDeck);
    spanDeck.innerHTML=(deck[i]);
    (deck[i]== 0) ? spanDeck.classList.add("zero") : "";
};


// Attribution des cartes

const cardsJ1 = [deck[0],deck[4]];
const cardsJ2 = [deck[1],deck[5]];
const cardsJ3 = [deck[2],deck[6]];
const cardsJ4 = [deck[3],deck[7]];

const cardsCommon = [ deck[9], deck[10], deck[11], deck[13], deck[15] ];

const mainJ1 = cardsJ1.concat(cardsCommon);
const mainJ2 = cardsJ2.concat(cardsCommon);
const mainJ3 = cardsJ3.concat(cardsCommon);
const mainJ4 = cardsJ4.concat(cardsCommon);

// Configuration manuelle de main

/* 
deck[0] = prompt("Carte A de J1 ?");
deck[4] = prompt("Carte B de J1 ?");
deck[1] = prompt("Carte A de J2 ?");
deck[5] = prompt("Carte B de J2 ?");
deck[2] = prompt("Carte A de J3 ?");
deck[6] = prompt("Carte B de J3 ?");
deck[3] = prompt("Carte A de J4 ?");
deck[7] = prompt("Carte B de J4 ?");
*/

// Affichage Cartes CSS

printCards(cardsJ1, mainJ1Container);
printCards(cardsJ2, mainJ2Container);
printCards(cardsJ3, mainJ3Container);
printCards(cardsJ4, mainJ4Container);

printCards(cardsCommon, divCartesCommunes);

// Calculs des mains des joueurs pour définir un score

/*calcul2Cartes(cardsJ1, scoreJ1);
calcul2Cartes(cardsJ2, scoreJ2);
calcul2Cartes(cardsJ3, scoreJ3);
calcul2Cartes(cardsJ4, scoreJ4);*/

calculMain(mainJ1, scoreJ1, mainJ1Container);
calculMain(mainJ2, scoreJ2, mainJ2Container);
calculMain(mainJ3, scoreJ3, mainJ3Container);
calculMain(mainJ4, scoreJ4, mainJ4Container); 

console.log(scoreJ1);
console.log(scoreJ2);
console.log(scoreJ3);
console.log(scoreJ4);

console.log( evalCards(scoreJ1), evalCards(scoreJ2), evalCards(scoreJ3), evalCards(scoreJ4) );

// Affichage des scores calculés

affichageScore(scoreJ1, scoreContainer, "J1");
affichageScore(scoreJ2, scoreContainer, "J2");
affichageScore(scoreJ3, scoreContainer, "J3");
affichageScore(scoreJ4, scoreContainer, "J4");

/*
affichageScore(scoreJ2, scoreContainer, "J2", nom = prompt("Nom Joueur 2 ?"));
affichageScore(scoreJ3, scoreContainer, "J3", nom = prompt("Nom Joueur 3 ?"));
affichageScore(scoreJ4, scoreContainer, "J4", nom = prompt("Nom Joueur 4 ?"));
*/

// Comparateur de scores

cpt = 4;

let comparateur = function(){

    // Figure

    console.log("--- FIGURE ---");

    comparateurScore([scoreJ1.figure, scoreJ2.figure, scoreJ3.figure, scoreJ4.figure] , interrupteurs );
  
    switch (cpt) {
        case 1 : return;
        case 0 : alert("Erreur ! Tous les interrupteurs sont activés !!!"); return;
        default :;
    }

    // Valeur

    console.log("--- VALEUR ---");

    for (j=0 ; j < 2 ; j++) {

        comparateurScore([scoreJ1.valeur[j], scoreJ2.valeur[j], scoreJ3.valeur[j], scoreJ4.valeur[j]] , interrupteurs );

        switch (cpt) {
            case 1 : return;
            case 0 : alert("Erreur ! Tous les interrupteurs sont activés !!!"); return;
            default :;
        }

    };

    // Couleur (pas essentiel)
/*
    console.log("--- COULEUR ---");

    comparateurScore([scoreJ1.couleur, scoreJ2.couleur, scoreJ3.couleur, scoreJ4.couleur] , interrupteurs );
 
    switch (cpt) {
        case 1 : return;
        case 0 : alert("Erreur ! Tous les interrupteurs sont activés !!!"); return;
        default :;
    }
*/
    // Kicker

    console.log("--- KICKER ---");

    for (j=0 ; j < 4 ; j++) {

        comparateurScore([scoreJ1.kicker[j], scoreJ2.kicker[j], scoreJ3.kicker[j], scoreJ4.kicker[j]] , interrupteurs );
       
        switch (cpt) {
            case 1 : return;
            case 0 : alert("Erreur ! Tous les interrupteurs sont activés !!!"); return;
            default :;
        }

    };

}();

// Annonce Gagnant

if (interrupteurs[0] == false) {
    let div = document.createElement("div");
    resultatContainer.appendChild(div);
    div.innerHTML=("J1 gagne !");
}

if (interrupteurs[1] == false) {
    let div = document.createElement("div");
    resultatContainer.appendChild(div);
    div.innerHTML=("J2 gagne !");
}

if (interrupteurs[2] == false) {
    let div = document.createElement("div");
    resultatContainer.appendChild(div);
    div.innerHTML=("J3 gagne !");
}

if (interrupteurs[3] == false) {
    let div = document.createElement("div");
    resultatContainer.appendChild(div);
    div.innerHTML=("J4 gagne !");
}
