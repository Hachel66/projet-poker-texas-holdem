document.addEventListener('DOMContentLoaded', function () {

  let domBtnBurger = document.getElementById("btn-burger");
  let domBtnBurgerMenu = document.getElementById("btn-burger-menu");

  let domBtnPlayers = document.getElementById("btn-playerlist");
  let liPlayers = document.querySelectorAll("li.li-player");

  // Bouton burger menu

  domBtnBurger.addEventListener('click', function () {

    let isMenuHidden = domBtnBurgerMenu.classList.contains('hidden');

    if (isMenuHidden) {

      domBtnBurgerMenu.classList.remove('hidden');
      domBtnBurger.classList.add('close');

    } else {

      domBtnBurgerMenu.classList.add('hidden');
      domBtnBurger.classList.remove('close');

    };

  });

  // Bouton Playerlist

  domBtnPlayers.addEventListener('click', function () {

    liPlayers.forEach(liPlayer => {

      let isPlayerHidden = liPlayer.classList.contains('hidden');

      if (isPlayerHidden) {

        liPlayer.classList.remove('hidden');
        domBtnPlayers.classList.add('close');

      } else {

        liPlayer.classList.add('hidden');
        domBtnPlayers.classList.remove('close');

      };

    });



  });

  // Bouton Chat/Log

  domBtnPlayers.addEventListener('click', function () {

    liPlayers.forEach(liPlayer => {

      let isPlayerHidden = liPlayer.classList.contains('hidden');

      if (isPlayerHidden) {

        liPlayer.classList.remove('hidden');
        domBtnPlayers.classList.add('close');

      } else {

        liPlayer.classList.add('hidden');
        domBtnPlayers.classList.remove('close');

      };

    });



  });

});