// Bouton Deck

let domBtnDeck = document.querySelector("#button-deck");

domBtnDeck.addEventListener('click', function () {

    let domDivDeck = document.querySelector('#deck');
    let isDeckHidden = domDivDeck.classList.contains('hidden');

    if (isDeckHidden) {

        domDivDeck.classList.remove('hidden');

    } else {

        domDivDeck.classList.add('hidden');

    };

});