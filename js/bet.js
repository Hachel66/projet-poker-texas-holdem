let deck = []; // Deck
const maxDeck = 52; // Nombre de cartes dans mon deck

// Paramètres générales du jeu
const PokerGame = function() {
    
    this.nbPlayers = 4;
    // this.nbPlayers = Math.random(1,8);
    this.timer = 60;
    this.turn = 0;
    this.blind = 100;
    this.cardsCommon = [];

    this.difficulty = 'medium';

};

// Paramètres des bots
const DataIA = function() {
    
    this.varRaise = 0;
    this.varFollow = 0;
    this.varFold = 0;

};

// Création de l'objet Score
const Score = function() {

    this.figure = null;
    this.valeur = [];
    this.couleur = null;
    this.kicker = [];

};

// Choix de la difficulté
/* PokerGame.difficulty = prompt("Choix de la difficulté (easy, medium, hard, master)"); */

// Création de l'objet Player
const Player = function() {
    
    this.username = null;
    this.coins = null;
    this.isBot = false;

    // Données propres au jeu

    this.bet = null;
    this.isDealer = false;
    this.cards = [];
    this.score = null;

};

// Création des différents Scores
const ScoreJ1 = new Score();
const ScoreJ2 = new Score();
const ScoreJ3 = new Score();
const ScoreJ4 = new Score();

// Création des différents Players
const Player1 = new Player([
    this.username = "Joueur 1",
    this.coins = 10000,
    this.score = ScoreJ1
]);
const Player2 = new Player([
    this.username = "BotA",
    this.coins = 10000,
    this.isBot = true,
    this.score = ScoreJ2
]);
const Player3 = new Player([
    this.username = "BotB",
    this.coins = 10000,
    this.isBot = true,
    this.score = ScoreJ3
]);
const Player4 = new Player([
    this.username = "BotC",
    this.coins = 10000,
    this.isBot = true,
    this.score = ScoreJ4
]);

// Mélange des cartes

for (i=0 ; i < maxDeck ; i++) {

    let deckBreak = false;

    do{

        calc = Math.floor( Math.random() * maxDeck );

        if (deck[calc]==undefined) {
            deck[calc] = i;
            deckBreak = true;
        }

    } while (deckBreak == false);

};

// Attribution des cartes

Player1.cards = [deck[0],deck[4]];
Player2.cards = [deck[1],deck[5]];
Player3.cards = [deck[2],deck[6]];
Player4.cards = [deck[3],deck[7]];

PokerGame.cardsCommon = [ deck[9], deck[10], deck[11], deck[13], deck[15] ];

const mainJ1 = Player1.cards.concat(PokerGame.cardsCommon);
const mainJ2 = Player2.cards.concat(PokerGame.cardsCommon);
const mainJ3 = Player3.cards.concat(PokerGame.cardsCommon);
const mainJ4 = Player4.cards.concat(PokerGame.cardsCommon);

// Evaluation des 2 premières cartes des joueurs

/*ScoreJ1 = calculMain(Player1.cartes, Player1.score);
ScoreJ2 = calculMain(Player2.cartes, Player2.score);
ScoreJ3 = calculMain(Player3.cartes, Player3.score);
ScoreJ4 = calculMain(Player4.cartes, Player4.score);*/

ScoreJ1 = calcul2Cartes(Player1.cartes, Player1.score);
ScoreJ2 = calcul2Cartes(Player2.cartes, Player2.score);
ScoreJ3 = calcul2Cartes(Player3.cartes, Player3.score);
ScoreJ4 = calcul2Cartes(Player4.cartes, Player4.score);

// Si score.figure == 1 : Les cartes ne sont pas de la même couleur
// Si score.figure == 2 : Les cartes sont de même valeur (Paire en main)
// Si score.figure == 3 : Les cartes sont de même couleur.

console.log( evalCards(ScoreJ1) );
console.log( evalCards(ScoreJ2) );
console.log( evalCards(ScoreJ3) );
console.log( evalCards(ScoreJ4) );