// Liste des fonctions du Poker

function printCards(main = [], container) { // Afficher cartes en CSS

    for (i = 0; i < main.length; i++) {

        let span = document.createElement("span");
        container.appendChild(span);

        calc = Math.floor(main[i] / 13);

        switch (calc) {
            case 0: span.classList.add("trefle");
                span.innerHTML = ("<i>♣</i><br>");
                break;
            case 1: span.classList.add("carreau");
                span.innerHTML = ("<i>♦</i><br>");
                break;
            case 2: span.classList.add("coeur");
                span.innerHTML = ("<i>♥</i><br>");
                break;
            case 3: span.classList.add("pique");
                span.innerHTML = ("<i>♠</i><br>");
                break;
            default: alert("Erreur : Pas de classe attribuable ! => " + calc);
        }

        calc = Math.floor(main[i] % 13 + 2); // donne une valeur entre 2 et 14

        switch (calc) {
            case 14: span.innerHTML += ("A<br>")
                break;
            case 13: span.innerHTML += ("K<br>")
                break;
            case 12: span.innerHTML += ("Q<br>")
                break;
            case 11: span.innerHTML += ("J<br>")
                break;
            default: span.innerHTML += (calc + "<br>");
        }

        span.innerHTML += ("<span>(" + main[i] + ")</span>");

    };

};

/* 
function createScoreObj (score = {}) { // Créer une nouvelle fiche de score

    score = {
    
        figure : null,
        valeur : [],
        couleur : null,
        kicker : [] 
    
    };

    return score;

};
 */

function evalCards (Score = {}) { 

    switch (Score.figure) {

        case 1 : { // Les cartes ne sont pas de même couleur

            switch (Score.valeur[0]) {

                case 14 : {

                    switch (true) {
                        case (Score.valeur[1] >= 10) : return 3;
                        case (Score.valeur[1] >= 7) : return 1;
                        default : return 0;
                    };

                };

                case 13 : {

                    switch (true) {
                        case (Score.valeur[1] >= 11) : return 3;
                        case (Score.valeur[1] == 10) : return 2;
                        case (Score.valeur[1] == 9) : return 1;
                        default : return 0;
                    };

                };
                    
                case 12 : {

                    switch (true) {
                        case (Score.valeur[1] >= 10) : return 2;
                        case (Score.valeur[1] == 9) : return 1;
                        default : return 0;
                    };

                };

                case 11 : {

                    switch (true) {
                        case (Score.valeur[1] == 10) : return 2;
                        case (Score.valeur[1] >= 8) : return 1;
                        default : return 0;
                    };

                };

                case 10 : {

                    switch (true) {
                        case (Score.valeur[1] >= 8) : return 1;
                        default : return 0;
                    };

                };

                case 9 : {

                    switch (true) {
                        case (Score.valeur[1] >= 7) : return 1;
                        default : return 0;
                    };

                };

                case 8 : {

                    switch (true) {
                        case (Score.valeur[1] == 7) : return 1;
                        default : return 0;
                    };

                };

                default : return 0;

            };

        }

        case 2 : { // Les cartes sont de même valeur (Paire en main)

            switch (true) {
                case (Score.valeur[0] >= 7) : return 3;
                case (Score.valeur[0] >= 5) : return 2;
                default : return 1;
            };

        }

        case 3 : { // Les cartes sont de même couleur

            switch (Score.valeur[0]) {

                case 14 : {

                    switch (true) {
                        case (Score.valeur[1] >= 10) : return 3;
                        case (Score.valeur[1] >= 6) : return 2;
                        default : return 1;
                    };

                };

                case 13 : {

                    switch (true) {
                        case (Score.valeur[1] >= 10) : return 3;
                        case (Score.valeur[1] == 9) : return 2;
                        default : return 1;
                    };

                };
                    
                case 12 : {

                    switch (true) {
                        case (Score.valeur[1] >= 10) : return 3;
                        case (Score.valeur[1] >= 8) : return 2;
                        default : return 0;
                    };

                };

                case 11 : {

                    switch (true) {
                        case (Score.valeur[1] >= 9) : return 3;
                        case (Score.valeur[1] == 8) : return 2;
                        case (Score.valeur[1] == 7) : return 1;
                        default : return 0;
                    };

                };

                case 10 : {

                    switch (true) {
                        case (Score.valeur[1] == 9) : return 3;
                        case (Score.valeur[1] == 8) : return 2;
                        case (Score.valeur[1] == 7) : return 1;
                        default : return 0;
                    };

                };

                case 9 : {

                    switch (true) {
                        case (Score.valeur[1] == 8) : return 2;
                        case (Score.valeur[1] >= 6) : return 1;
                        default : return 0;
                    };

                };

                case 8 : {

                    switch (true) {
                        case (Score.valeur[1] >= 6) : return 1;
                        default : return 0;
                    };

                };

                case 7 : {

                    switch (true) {
                        case (Score.valeur[1] >= 5) : return 1;
                        default : return 0;
                    };

                };
                
                case 6 : {

                    switch (true) {
                        case (Score.valeur[1] == 5) : return 1;
                        default : return 0;
                    };

                };

                case 5 : {

                    switch (true) {
                        case (Score.valeur[1] == 4) : return 1;
                        default : return 0;
                    };

                };

                default : return 0;

            };

        }
    }

}

function calcul2Cartes(cartes = [], score = {}) { // Calcul cartes initiales

    // Reset

    i = j = k = cpt = null;
    calc = [];

    // ------------ Paire en main ?

    for (i = 0; i < cartes.length; i++) {
        calc[i] = Math.floor(cartes[i] % 13 + 2);
    }

    if (calc[0] == calc[1]) {
        score.valeur[0] = calc[0]
        score.figure = 2
    }

    // ------------ Même couleur ?

    if (score.figure != 2) {

        calc = [];

        for (i = 0; i < cartes.length; i++) {
            calc[i] = Math.floor(cartes[i] / 13);
        };

        if (calc[0] == calc[1]) {

            score.figure = 3;
            score.couleur = calc[0];
            score.valeur = [];

            for (i = 0; i < calc.length; i++) {

                if (calc[i] == score.couleur) score.valeur.push((cartes[i] % 13) + 2);

            }

            score.valeur.sort(function (a, b) {
                return a - b;
            });
            score.valeur.reverse();
        }

    };

    // ------------ Cartes dissociées

    if (score.figure == null) {

        calc = Object.assign([], cartes);

        for (i = 0; i < cartes.length; i++) {
            calc[i] = Math.floor(calc[i] % 13 + 2);
        };

        calc.sort(function (a, b) {
            return a - b;
        });

        calc.reverse();

        score.figure = 1;
        score.valeur = [calc[0], calc[1]];

    };

    // ---------------- Cutting borders !

    score.valeur.length = 2;
    return score;

};

function calculMain(main = [], score = {}, container) { // Calcul du score

    // Reset

    i = j = k = cpt = null;
    calc = [];

    // ------------ Valeurs communes ?

    for (i = 0; i < main.length; i++) {
        calc[i] = Math.floor(main[i] % 13 + 2);
    }

    for (i = 0; i < calc.length; i++) {

        for (j = i + 1; j < calc.length; j++) {

            if (calc[i] == calc[j]) {

                if (score.valeur[0] == null) {

                    score.valeur[0] = calc[i]

                } else if (score.valeur[1] == null && calc[i] != score.valeur[0]) {

                    score.valeur[1] = calc[i]

                } else if (calc[i] != score.valeur[0] && calc[i] != score.valeur[1]) {

                    score.valeur[2] = calc[i]

                };

            };

        };

    };

    score.valeur.sort(function (a, b) {
        return a - b;
    });

    score.valeur.reverse();

    // ----------------------- Combien de fois ?

    cpt = 0;

    for (j = 0; j < score.valeur.length; j++) {

        for (i = 0; i < calc.length; i++) {

            if (calc[i] == score.valeur[j]) cpt++;

        }

        switch (cpt) {

            case 4: score.figure = 8

                if (j > 0) {
                    [score.valeur[0], score.valeur[j]] = [score.valeur[j], score.valeur[0]];
                }

                break;

            case 3:

                if (score.figure == null) { score.figure = 4 }

                else {
                    score.figure = 7

                    if (j > 0) {
                        [score.valeur[0], score.valeur[j]] = [score.valeur[j], score.valeur[0]];
                    }

                };

                break;

            case 2:

                if (score.figure == null) {

                    score.figure = 2
                }

                else {
                    switch (score.figure) {
                        case 4:

                            score.figure = 7;
                            break;

                        case 2:

                            score.figure = 3;
                            break;

                    };

                };

                break;


        };

        cpt = 0;

    };

    // ------------ Suite

    calc.sort(function (a, b) {
        return a - b;
    });
    calc.reverse();

    cpt = 0

    for (i = 0; i < (calc.length - 1); i++) {

        if ((calc[i] - calc[i + 1]) == 1) cpt++;
        else if ((calc[i] - calc[i + 1]) != 0) cpt = 0;

        if (cpt == 4) {
            score.figure = 5;
            score.valeur = [calc[i] + 3];
            break;
        }

    };

    // ------------ Couleur

    calc = [];

    for (i = 0; i < main.length; i++) {
        calc[i] = Math.floor(main[i] / 13);
    };

    for (i = 0; i < 4; i++) {

        cpt = 0;

        for (j = 0; j < calc.length; j++) {

            if (calc[j] == i) cpt++;

        }

        if (cpt >= 5) {

            score.figure = 6;
            score.couleur = i;
            score.valeur = [];

            for (k = 0; k < calc.length; k++) {

                (calc[k] == score.couleur) ? score.valeur.push((main[k] % 13) + 2) : "";

            }

            score.valeur.sort(function (a, b) {
                return a - b;
            });
            score.valeur.reverse();

            break;
        }

    };

    // ------------ Quinte Flush


    calc = Object.assign([], main);

    calc.sort(function (a, b) {
        return a - b;
    });
    calc.reverse();

    cpt = 0

    for (i = 0; i < (calc.length - 1); i++) {

        if ((calc[i] - calc[i + 1]) == 1) cpt++;
        else if ((calc[i] - calc[i + 1]) != 0) cpt = 0;

        if (cpt == 4) {

            score.valeur = [(((parseFloat(calc[i]) + 3) % 13) + 2)];
            (score.valeur[0] == 14) ? score.figure = 10 : score.figure = 9;
            break;

        }

    };

    // ------------ Carte Haute

    if (score.figure == null) {

        console.log("Main :");
        console.log(main);

        calc = Object.assign([], main);

        for (i = 0; i < main.length; i++) {
            calc[i] = Math.floor(calc[i] % 13 + 2);
        };

        calc.sort(function (a, b) {
            return a - b;
        });

        calc.reverse();

        score.figure = 1;
        score.valeur = [calc[0]];

    };

    // ---------------- Time to kick !

    for (i = 0; i < main.length; i++) {
        calc[i] = Math.floor(main[i] % 13 + 2);
    };

    calc.sort(function (a, b) {
        return a - b;
    });

    calc.reverse();


    for (i = 0; i < calc.length; i++) {

        (score.valeur.indexOf(calc[i]) == -1) ? score.kicker.push(calc[i]) : "";

    };

    // Certaines figures ne comptent pas de kicker, on les enlève donc pour éviter de les prendre en compte par inadvertance
    // Certaines doivent avoir un kicker limité !


    switch (score.figure) {

        case 1: score.kicker.length = 4; break;
        case 2: score.kicker.length = 3; break;
        case 3: score.kicker.length = 1; break;
        case 4: score.kicker.length = 2; break;
        case 5: score.kicker = [null]; break;
        case 6: score.kicker = score.valeur; score.valeur = score.kicker.splice(0, 1); break;
        case 7: score.kicker = [null]; break;
        case 8: score.kicker.length = 1; break;
        case 9: score.kicker = [null]; break;
        case 10: score.kicker = [null]; break;

    }

    /*     
    
        console.log("Main :");
        console.log(main);
    
        console.log("Calc :");
        console.log(calc);
    
        console.log("Kicker :");
        console.log(score.kicker);
    
     */

    console.log("Joueur OK");

    return score;

};

function affichageScore(score = {}, container, joueurID = "", nom = "") { // Affichage textuel du score du joueur

    let div = document.createElement("div");
    container.appendChild(div);

    (nom == "") ? nom = joueurID : "";
    let debutPhrase = (nom + " a ");
    (joueurID == "J1") ? debutPhrase = "Vous avez " : "";

    switch (score.figure) {

        case 1: div.innerHTML = (debutPhrase + "une " + nomFigure[score.figure] + " ! (Valeur haute : " + nomValeur[score.valeur[0]] + ")"); break;
        case 2: div.innerHTML = (debutPhrase + "une " + nomFigure[score.figure] + " de " + nomValeur[score.valeur[0]] + " !"); break;
        case 3: div.innerHTML = (debutPhrase + nomFigure[score.figure] + " de " + nomValeur[score.valeur[0]] + " et de " + nomValeur[score.valeur[1]] + " !"); break;
        case 4: div.innerHTML = (debutPhrase + "un " + nomFigure[score.figure] + " de " + nomValeur[score.valeur[0]] + " !"); break;
        case 5: div.innerHTML = (debutPhrase + "une " + nomFigure[score.figure] + " ! (Valeur haute : " + nomValeur[score.valeur[0]] + ")"); break;
        case 6: div.innerHTML = (debutPhrase + "une " + nomFigure[score.figure] + " ! (" + nomCouleur[score.couleur] + ")"); break;
        case 7: div.innerHTML = (debutPhrase + "un " + nomFigure[score.figure] + " de " + nomValeur[score.valeur[0]] + " sur " + nomValeur[score.valeur[1]] + " !"); break;
        case 8: div.innerHTML = (debutPhrase + "un " + nomFigure[score.figure] + " de " + nomValeur[score.valeur[0]] + " !"); break;
        case 9: div.innerHTML = (debutPhrase + "une " + nomFigure[score.figure] + " !! (Valeur haute : " + nomValeur[score.valeur[0]] + " ; " + nomCouleur[score.couleur] + ")"); break;
        case 10: div.innerHTML = (debutPhrase + "une " + nomFigure[score.figure] + " !!! (" + nomCouleur[score.couleur] + ")"); break;
        default: div.innerHTML = ("Impossible de déterminer votre main..."); break;

    };

    let span = document.createElement("span");
    div.appendChild(span);
    span.innerHTML = (joueurID);

};

function comparateurScore(value = [], interr = []) {

    (value.length != interr.length) ? alert("Erreur ! Le comparateur doit avoir autant de Valeurs que d'Interrupteurs !") : "";

    console.log(value);

    if (value.every((i) => i == undefined)) {

        console.log("skip sucessful");
        return;

    }

    // On ne prend que les valeurs des joueurs encore en lice pour calculer le maximum.

    calc = [];

    for (i = 0; i < value.length; i++) {

        if (interr[i] == false) { // si pas couché

            if (value[i] != undefined) calc.push(value[i]);

        }

    }

    calc = Math.max(...calc);

    console.log("Le maximum trouvé est de : " + calc);

    // On active l'interrupteur "Couché" si le joueur a une valeur inférieure au maximum calculé (si il n'est pas déjà couché).

    for (i = 0; i < interr.length; i++) {

        if (value[i] == null || value[i] == undefined) value[i] = 0;

        if ((value[i] < calc) && (interr[i] == false)) {

            interr[i] = true;
            cpt--;

        };

    };

    console.log(interr);

    return interr;

};